package com.dhc.tika.Data;


import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.de.GermanAnalyzer;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.miscellaneous.ASCIIFoldingFilter;
import org.apache.lucene.analysis.standard.ClassicFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.ngram.EdgeNGramTokenizer;

import java.io.IOException;
import java.io.StringReader;
import java.util.*;

public class Tokenizer {

  static List<String> getTokens(String fullText) throws IOException {
    TokenStream tokenStream = null;
    List<String> Tokens = new ArrayList<String>();
    try {

      StandardTokenizer stdToken = new StandardTokenizer();
      stdToken.setReader(new StringReader(fullText));


      CharArraySet stopWordsEnglish = EnglishAnalyzer.getDefaultStopSet();

      CharArraySet stopWordsGerman = GermanAnalyzer.getDefaultStopSet();

      stopWordsGerman.addAll(stopWordsEnglish);

      tokenStream = new StopFilter(new ASCIIFoldingFilter(new ClassicFilter(new LowerCaseFilter(stdToken))), stopWordsGerman);

      tokenStream.reset();

      CharTermAttribute token = tokenStream.getAttribute(CharTermAttribute.class);

      while (tokenStream.incrementToken()) {
        String term = token.toString();
        StringReader stringReader = new StringReader(term);
        EdgeNGramTokenizer tokenizer = new EdgeNGramTokenizer(2, 10);
        tokenizer.setReader(stringReader);
        tokenizer.reset();
        CharTermAttribute termAtt = tokenizer.getAttribute(CharTermAttribute.class);

        while (tokenizer.incrementToken()) {
          String ngramsTokens = termAtt.toString();
          if (Tokens.contains(ngramsTokens)) {
            continue;
          }
          Tokens.add(ngramsTokens);
        }

      }
      return Tokens;

    } finally {
      if (tokenStream != null) {
        try {
          tokenStream.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }

    }

  }

}
