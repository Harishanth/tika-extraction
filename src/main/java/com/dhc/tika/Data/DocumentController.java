package com.dhc.tika.Data;

import org.apache.tika.exception.TikaException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import org.xml.sax.SAXException;


@RestController
@RequestMapping(path = "api/v1/document")
public class DocumentController {

  private final DocumentService documenService;

  @Autowired
  public DocumentController(DocumentService documentService){
    this.documenService = documentService;
  }


  @GetMapping
  public List<Documentcontent>getDocument(){
    return documenService.getDocument();
  }

  @PostMapping
  public Documentcontent parseDocument(@RequestBody String file) throws IOException, TikaException, SAXException {
    return documenService.parseDocument(file);
  }

  }
