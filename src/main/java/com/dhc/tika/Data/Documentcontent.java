package com.dhc.tika.Data;
import org.apache.commons.lang3.StringUtils;

public class Documentcontent {
  private String content;
  private String contentType;
  private String metadata;
  private String ngram;
  private String keywords;

  public Documentcontent() {
    content = StringUtils.EMPTY;
    contentType = StringUtils.EMPTY;
    metadata = StringUtils.EMPTY;
    ngram = StringUtils.EMPTY;
    keywords = StringUtils.EMPTY;

  }

  public Documentcontent(String content, String contentType, String metadata, String ngram, String keywords) {
    this.content = content;
    this.contentType = contentType;
    this.metadata = metadata;
    this.ngram = ngram;
    this.keywords= keywords;

  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getContentType() {
    return contentType;
  }

  public void setContentType(String contentType) {
    this.contentType = contentType;
  }
  public String getMeta() {
    return metadata;
  }

  public void setMetadata(String metadata) {
    this.metadata = metadata;
  }

  public String getNgram() {
    return ngram;
  }

  public void setNgram(String content) {
    this.ngram = ngram;
  }

  public String getKeywords() {
    return keywords;
  }

  public void setKeywords(String Keywords) {
    this.keywords = keywords;
  }


}
