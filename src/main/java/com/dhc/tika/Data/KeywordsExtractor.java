package com.dhc.tika.Data;

import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.de.GermanAnalyzer;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.en.PorterStemFilter;
import org.apache.lucene.analysis.miscellaneous.ASCIIFoldingFilter;
import org.apache.lucene.analysis.standard.ClassicFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

import java.io.IOException;
import java.io.StringReader;
import java.util.*;

/**
 * Keywords extractor functionality handler
 */
class KeywordsExtractor {

  /**
   * Get list of keywords with stem form, frequency rank, and terms dictionary
   *
   * @param fullText
   * @return List<CardKeyword>, which contains keywords cards
   * @throws IOException
   */
  static List<CardKeyword> getKeywordsList(String fullText) throws IOException {

    TokenStream tokenStream = null;

    try {

      StandardTokenizer stdToken = new StandardTokenizer();
      stdToken.setReader(new StringReader(fullText));

      CharArraySet stopWordsEnglish = EnglishAnalyzer.getDefaultStopSet();

      CharArraySet stopWordsGerman = GermanAnalyzer.getDefaultStopSet();

      //this contains stopwords in German and English
      stopWordsGerman.addAll(stopWordsEnglish);

      tokenStream = new StopFilter(new ASCIIFoldingFilter(new ClassicFilter(new LowerCaseFilter(stdToken))), stopWordsGerman);

      tokenStream.reset();

      List<CardKeyword> cardKeywords = new LinkedList<>();

      CharTermAttribute token = tokenStream.getAttribute(CharTermAttribute.class);

      while (tokenStream.incrementToken()) {

        String term = token.toString();
        String stem = getStemForm(term);

        if (stem != null) {
          CardKeyword cardKeyword = find(cardKeywords, new CardKeyword(stem.replaceAll("-0", "-")));
          // treat the dashed words back, let look them pretty
          cardKeyword.add(term.replaceAll("-0", "-"));
        }
      }

      // reverse sort by frequency
      Collections.sort(cardKeywords);

      return cardKeywords;
    } finally {
      if (tokenStream != null) {
        try {
          tokenStream.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
  }

  /**
   * Get stem form of the term
   *
   * @param term
   * @return String, which contains the stemmed form of the term
   * @throws IOException
   */
  private static String getStemForm(String term) throws IOException {

    TokenStream tokenStream = null;

    try {
      StandardTokenizer stdToken = new StandardTokenizer();
      stdToken.setReader(new StringReader(term));

      tokenStream = new PorterStemFilter(stdToken);
      tokenStream.reset();

      // eliminate duplicate tokens by adding them to a set
      Set<String> stems = new HashSet<>();

      CharTermAttribute token = tokenStream.getAttribute(CharTermAttribute.class);

      while (tokenStream.incrementToken()) {
        stems.add(token.toString());
      }

      // if stem form was not found or more than 2 stems have been found, return null
      if (stems.size() != 1) {
        return null;
      }

      String stem = stems.iterator().next();

      // if the stem form has non-alphanumerical chars, return null
      if (!stem.matches("[a-zA-Z0-9-]+")) {
        return null;
      }

      return stem;
    } finally {
      if (tokenStream != null) {
        try {
          tokenStream.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
  }

  /**
   * Find sample in collection
   *
   * @param collection
   * @param sample
   * @param <T>
   * @return <T> T, which contains the found object within collection if exists, otherwise the initially searched object
   */
  private static <T> T find(Collection<T> collection, T sample) {

    for (T element : collection) {
      if (element.equals(sample)) {
        return element;
      }
    }

    collection.add(sample);

    return sample;
  }
}
