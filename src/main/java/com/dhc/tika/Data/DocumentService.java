package com.dhc.tika.Data;

import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.EmptyParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;
import static org.apache.tika.metadata.HttpHeaders.CONTENT_TYPE;


@Service
public class DocumentService {

  public List<Documentcontent> getDocument() {
    return List.of(
      new Documentcontent(
        "HaRI",
        "Test content",
        "meta",
        "ngram",
        "Keywords"
      )
    );
  }


  public Documentcontent parseDocument(String file) throws IOException, TikaException, SAXException {

    BodyContentHandler handler = new BodyContentHandler(-1);
    Metadata metadata = new Metadata();

    //https://issues.apache.org/jira/browse/TIKA-2096
    ParseContext context = new ParseContext();
    context.set(Parser.class, new EmptyParser());

    //TikaConfig config = new TikaConfig("C:\\Users\\sivakumaran\\Downloads\\tika\\tika\\src\\main\\tika-config.xml");
    //Detector detector = config.getDetector();

    AutoDetectParser autodetectparser = new AutoDetectParser();


    File filetest = new File(file);


    InputStream inputstream = new FileInputStream(filetest);


    autodetectparser.parse(inputstream, handler, metadata, context);

    String content = handler.toString().replace("\n", "");

    // treat the dashed words, don't let separate them during the processing
    content = content.replaceAll("-+", "-0");

    // replace any punctuation char but apostrophes and dashes with a space
    content = content.replaceAll("[\\p{Punct}&&[^'-]]+", " ");

    // replace most common English contractions
    content = content.replaceAll("(?:'(?:[tdsm]|[vr]e|ll))+\\b", "");

    // replace Numbers
    content = content.replaceAll("[0123456789]", "");

    content = content.replaceAll("\\t", "");

    List<CardKeyword> keywordsList = KeywordsExtractor.getKeywordsList(content);

    List<String> Tokens = Tokenizer.getTokens(content);

    Dictionary keywords = new Hashtable();

    for (int i = 0; i < Math.min(10, keywordsList.size()); i++) {
      keywords.put(keywordsList.get(i).getStem(), keywordsList.get(i).getFrequency());
    }

    return new Documentcontent(content, metadata.get(CONTENT_TYPE), metadata.toString(), Tokens.toString(), keywords.toString());

  }

}
